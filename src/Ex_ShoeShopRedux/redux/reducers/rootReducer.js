import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShopReducer";
export const rootReducer_shoeShop = combineReducers({
    shoeShopReducer,
});
